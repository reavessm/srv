/**
 * File: main.go
 * Written by: Stephen M. Reaves
 * Created on: Fri, 12 Aug 2022
 */

package main

import (
	"fmt"
	"net/http"
	"os"
	"strconv"
)

func main() {
	if len(os.Args) != 2 {
		fmt.Printf("Usage %s: <portNumber>\n", os.Args[0])
		os.Exit(1)
	}

	port, parseError := strconv.Atoi(os.Args[1])
	if parseError != nil {
		fmt.Printf("Error! %v\n", parseError)
		os.Exit(2)
	}

	if err := run(port); err != nil {
		fmt.Printf("Error! %v\n", err)
		os.Exit(3)
	}
}

func run(port int) error {
	mux := http.NewServeMux()

	mux.Handle("/", http.FileServer(http.Dir("./static")))

	handler := http.Server{
		Handler: mux,
		Addr:    ":8080",
	}

	fmt.Printf("Serving at http://localhost:%d\n", port)
	return handler.ListenAndServe()
}
